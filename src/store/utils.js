import { STORAGE_KEY_TOKEN, STORAGE_KEY_PROJECT_ID } from './constants';

export const getToken = () => {
  return localStorage.getItem(STORAGE_KEY_TOKEN);
}

export const setToken = (token) => {
  localStorage.setItem(STORAGE_KEY_TOKEN, token);
}

export const getProjectId = () => {
  return parseInt(localStorage.getItem(STORAGE_KEY_PROJECT_ID), 10) || 0;
}

export const setProjectId = (projectId) => {
  localStorage.setItem(STORAGE_KEY_PROJECT_ID, projectId);
}

export const setCache = (key, data) => {
  let cache =  getCache();

  if (!cache) {
    cache = {};
  }

  cache[key] = data;
  localStorage.setItem(getProjectId(), JSON.stringify(cache));
}

export const getCache = (key) => {
  const cache =  localStorage.getItem(getProjectId());
  return JSON.parse(cache);
}
